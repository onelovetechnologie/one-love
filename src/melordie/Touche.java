/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package melordie;

import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.scene.Parent;
import javafx.scene.effect.Light;
import javafx.scene.effect.Lighting;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

/**
 *
 * @author HP
 */
public class Touche extends Parent{
    public String lettre = new String("X");
    private Instru ins;
    private int positionX = 0;
    private int positionY = 0;
    private int note= 0;
    Rectangle fondTuche;
    Text letrTouche;

    public Touche(String l, int posX, int posY ,int n,Instru inst) {
        lettre = l;
        positionX = posX;
        positionY = posY;
        note = n;
        ins = inst;
        fondTuche = new Rectangle(75, 75, Color.WHITE);
        fondTuche.setArcHeight(10);
        fondTuche.setArcWidth(10);
        this.getChildren().add(fondTuche);
        
        letrTouche = new Text(lettre);
       letrTouche.setFont(new Font(25));
       letrTouche.setX(25);
       letrTouche.setY(45);
       letrTouche.setFill(Color.GRAY);
        Light.Distant light = new  Light.Distant();
        light.setAzimuth(-45.0);
        Lighting li = new Lighting();
        li.setLight(light);
        fondTuche.setEffect(li);
      
       this.getChildren().add(letrTouche);
       this.setTranslateX(positionX);
       this.setTranslateY(positionY);
       this.setOnMouseEntered(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                fondTuche.setFill(Color.CADETBLUE);
            }
        });
       this.setOnMouseExited(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
               fondTuche.setFill(Color.WHITE);
            }
        });
      this.setOnMousePressed(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent me) {
               appuyer();
            }
        });
      this.setOnMouseReleased(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent me) {
                relacher();
            }
        });
    }
    
    
 
    public void appuyer(){
        fondTuche.setFill(Color.CADETBLUE);
        this.setTranslateY(positionY+2);
        ins.note_on(note);
       } 
    

    public void relacher(){
          fondTuche.setFill(Color.WHITE);
          this.setTranslateY(positionY);
          ins.note_off(note);
       }
    
    
}
