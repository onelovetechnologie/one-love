/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package melordie;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;

/**
 *
 * @author HP
 */
public class Melordie extends Application {
    
    @Override
    public void start(Stage primaryStage) {
       
        Group root = new Group();
        Scene scene = new Scene(root, 500, 500, Color.DARKBLUE);
        Instru monInstru = new Instru();
       // monInstru.note_on(10);
        Clavier monClavier = new Clavier(monInstru);
        monClavier.requestFocus();
        ChangeInstrue chins = new ChangeInstrue();
         Circle cercle = new Circle();
        cercle.setCenterX(100);
        cercle.setCenterY(100);
        cercle.setRadius(100);
        cercle.setFill(Color.YELLOW);
        cercle.setStroke(Color.ORANGE);
        cercle.setStrokeWidth(5);
        root.getChildren().add(cercle);
        root.getChildren().add(chins);
        root.getChildren().add(monClavier);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Application.launch(Melordie.class,args);
    }
    
}
