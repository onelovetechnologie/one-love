/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package melordie;


import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.effect.Reflection;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Rectangle;

/**
 *
 * @author HP
 */
public class Clavier extends Parent{
    private Touche[] touches;
    private Instru ins;

    public Clavier(Instru ins) {
        this.ins = ins;
         Rectangle cadreClavier = new Rectangle();
        cadreClavier.setWidth(400);
        cadreClavier.setHeight(200);
        cadreClavier.setArcHeight(30);
        cadreClavier.setArcWidth(30);
        cadreClavier.setFill(new LinearGradient(0f,0f , 0f, 1f, true, CycleMethod.NO_CYCLE,
                new Stop[]{
                    new Stop(0, Color.web("#333333")),
                    new Stop(1, Color.web("#000000"))
                }
            )
        );
        
        Reflection r = new Reflection();
        r.setFraction(0.25);
        r.setBottomOpacity(0);
        r.setTopOpacity(0.5);
        cadreClavier.setEffect(r);
        
        touches = new Touche[]{
            new Touche("U", 50, 20,60,ins),
            new Touche("I", 128, 20, 62,ins),
            new Touche("0", 206, 20, 64,ins),
            new Touche("P", 284, 20, 65,ins),
            new Touche("J", 75, 98, 67,ins),
            new Touche("K", 153, 98, 69,ins),
            new Touche("L", 231, 98, 71,ins),
            new Touche("M", 309, 98, 72,ins)
        };
        this.setTranslateX(50);
        this.setTranslateY(250);
        this.getChildren().add(cadreClavier);
        
        for(Touche touch: touches){
            this.getChildren().add(touch);
        }
        
        this.setOnKeyPressed(new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent ke) {
               for(Touche touche: touches){
                   if (touche.lettre.equals(ke.getText().toUpperCase())) {
                       touche.appuyer();
                       
                   }
               }
            }
        });
        
        this.setOnKeyReleased(new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent ke) {
                 for(Touche touche: touches){
                       if (touche.lettre.equals(ke.getText().toUpperCase())) {
                       touche.relacher();
                       
                   }
               }
            }
        });
    }
    
    
    
    
    public Clavier() {
        Rectangle cadreClavier = new Rectangle();
        cadreClavier.setWidth(400);
        cadreClavier.setHeight(200);
        cadreClavier.setArcHeight(30);
        cadreClavier.setArcWidth(30);
        cadreClavier.setFill(new LinearGradient(0f,0f , 0f, 1f, true, CycleMethod.NO_CYCLE,
                new Stop[]{
                    new Stop(0, Color.web("#333333")),
                    new Stop(1, Color.web("#000000"))
                }
            )
        );
        
        Reflection r = new Reflection();
        r.setFraction(0.25);
        r.setBottomOpacity(0);
        r.setTopOpacity(0.5);
        cadreClavier.setEffect(r);
        
        touches = new Touche[]{
            new Touche("U", 50, 20,60,ins),
            new Touche("I", 128, 20, 62,ins),
            new Touche("0", 206, 20, 64,ins),
            new Touche("P", 284, 20, 65,ins),
            new Touche("J", 75, 98, 67,ins),
            new Touche("K", 153, 98, 69,ins),
            new Touche("L", 231, 98, 71,ins),
            new Touche("M", 309, 98, 72,ins)
        };
        this.setTranslateX(50);
        this.setTranslateY(250);
        this.getChildren().add(cadreClavier);
        
        for(Touche touch: touches){
            this.getChildren().add(touch);
        }
    }
    

  
    }


 
   
    

